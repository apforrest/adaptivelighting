#!/bin/bash

DATE=`date +%Y-%m-%d:%H:%M:%S`

STATUS=`curl -s -o /dev/null -w "%{http_code}" http://raspi.teamcurie.org:5000/light/group/1`
echo $DATE 'Adaptive light server response status:' $STATUS

if [[ $STATUS -ne 200 ]]; then
   kill -9 `ps -ef | grep python | grep -v grep | awk '{ print $2 }'`

   echo 'Unhappy response code, restarting adaptive light server'
   python /development/adaptivelighting/adaptiveserv.py >> /development/adaptivelighting/logs/server.log 2>&1 &
fi

