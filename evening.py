import datetime

import requests

print datetime.datetime.now().isoformat() + ' turning lights off for the evening'

resp = requests.get('http://raspi.teamcurie.org:5000/light/group/1?status=off')
resp.raise_for_status()
