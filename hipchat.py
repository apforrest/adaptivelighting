import datetime

import requests


def contains_poke():
    time_shift = datetime.datetime.now() - datetime.timedelta(minutes=1)
    iso_time_stamp = time_shift.isoformat()

    headers = {'Authorization': 'Bearer XL1uje5kLJUo3j8QTWiKAqcshpWLj3u4nbytUInH'}
    hipchat_url = 'http://api.hipchat.com/v2/room/Team%20Curie/history?reverse=false&end-date=' + iso_time_stamp

    hipchat_response = requests.get(hipchat_url, headers=headers)
    hipchat_response.raise_for_status()
    data = hipchat_response.json()

    items = data['items']

    for item in items:
        if item['type'] != 'message':
            continue

        if not item['message']:
            continue

        message = item['message']
        if not ('@all' in message or '@here' in message):
            continue

        return True

    return False


if contains_poke():
    print datetime.datetime.now().isoformat() + ' signalling hipchat notification'

    url = 'http://raspi.teamcurie.org:5000/light/group/1'

    resp = requests.get(url)
    resp.raise_for_status()
    light_status = resp.json()

    requests.get(url + '?status=on')
    requests.get(url + '?colour=240')

    requests.get(url + '?brightness=100')
    requests.get(url + '?brightness=1')
    requests.get(url + '?brightness=100')
    requests.get(url + '?brightness=1')
    requests.get(url + '?brightness=100')
    requests.get(url + '?brightness=1')
    requests.get(url + '?brightness=100')

    previous_colour = 'white' if not light_status['colour'] else str(light_status['colour'])
    requests.get(url + '?colour=' + previous_colour)

    previous_brightness = '100' if not light_status['brightness'] else str(light_status['brightness'])
    requests.get(url + '?brightness=' + previous_brightness)

    if light_status['status'] and light_status['status'] == 'off':
        requests.get(url + '?status=off')
