from flask import Flask, jsonify, request, abort, make_response

from adaptivelights import *

app = Flask(__name__)
lights = AdaptiveLights('172.16.107.255', 8899)


@app.route('/light/group/<group>', methods=['GET'])
def group_controller(group):
    try:
        current_group = lights.get_group(group)

        status = request.args.get('status')
        colour = request.args.get('colour')
        brightness = request.args.get('brightness')

        if status:
            change_group_status(current_group, status)
        elif colour:
            current_group.set_colour(parse_to_int_if_possible(colour))
        elif brightness:
            current_group.set_brightness(parse_to_int_if_possible(brightness))
        return jsonify(current_group.to_json())

    except ValueError as error:
        abort(400, str(error))


def change_group_status(current_group, status):
    if status == 'on':
        current_group.on()
    else:
        current_group.off()


def parse_to_int_if_possible(potential_int):
    try:
        return int(potential_int)
    except ValueError:
        return potential_int


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': error.description}), 400)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
