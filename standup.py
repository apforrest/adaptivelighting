import datetime

import requests

print datetime.datetime.now().isoformat() + ' signalling stand up'

url = 'http://raspi.teamcurie.org:5000/light/group/1'

resp = requests.get(url)
resp.raise_for_status()
light_status = resp.json()

requests.get(url + '?status=on')
requests.get(url + '?colour=120')

requests.get(url + '?brightness=100')
requests.get(url + '?brightness=1')
requests.get(url + '?brightness=100')
requests.get(url + '?brightness=1')
requests.get(url + '?brightness=100')
requests.get(url + '?brightness=1')
requests.get(url + '?brightness=100')

previous_colour = 'white' if not light_status['colour'] else str(light_status['colour'])
requests.get(url + '?colour=' + previous_colour)

previous_brightness = '100' if not light_status['brightness'] else str(light_status['brightness'])
requests.get(url + '?brightness=' + previous_brightness)

if light_status['status'] and light_status['status'] == 'off':
    requests.get(url + '?status=off')
