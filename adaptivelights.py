import socket
import time

GROUPS = {
    '*': {
        'off': 0x41,
        'on': 0x42,
        'white': 0xC2
    },
    '1': {
        'off': 0x46,
        'on': 0x45,
        'white': 0xC5
    },
    '2': {
        'off': 0x48,
        'on': 0x47,
        'white': 0xC7
    },
    '3': {
        'off': 0x4A,
        'on': 0x49,
        'white': 0xC9
    },
    '4': {
        'off': 0x4C,
        'on': 0x4B,
        'white': 0xCB
    }
}

BLANK = 0x00
COLOUR = 0x40
BRIGHTNESS = 0x4E


class BasicGroup:
    """The basic implementation of a group"""

    def __init__(self, id, controller):
        self.id = id
        self.controller = controller
        self.status = 'on'

    def on(self):
        self.status = 'on'
        self.controller.send(GROUPS[self.id]['on'], BLANK)

    def off(self):
        self.status = 'off'
        self.controller.send(GROUPS[self.id]['off'], BLANK)

    def set_colour(self, colour):
        raise ValueError('colour change is not supported')

    def set_brightness(self, brightness):
        raise ValueError('brightness change is not supported')

    def to_json(self):
        return {'id': self.id, 'status': self.status}


class ComplexGroup(BasicGroup):
    def __init__(self, id, controller):
        BasicGroup.__init__(self, id, controller)
        self.colour = 'white'
        self.brightness = 100

    def set_colour(self, colour):
        if (colour < 0 or colour > 360) and colour != 'white':
            raise ValueError('colour has to be between 0 and 360, or white')

        self.colour = colour
        if colour == 'white':
            self.controller.send(GROUPS[self.id]['white'], BLANK)
        else:
            colour_offset = 360 - ((colour - 246) % 360)
            colour_index = int((colour_offset / 360.0) * 255)
            self.controller.send(COLOUR, colour_index)

    def set_brightness(self, brightness):
        if brightness < 1 or brightness > 100:
            raise ValueError('brightness must be a valid percentage')

        self.brightness = brightness
        brightness_index = int(((brightness / 100.0) * 25) + 2)
        self.controller.send(BRIGHTNESS, brightness_index)

    def to_json(self):
        json = BasicGroup.to_json(self)
        json['colour'] = self.colour
        json['brightness'] = self.brightness
        return json


class AdaptiveLights:
    def __init__(self, ip, port):
        self.__ip = ip
        self.__port = port
        self.__groups = {
            '*': BasicGroup('*', self),
            '1': ComplexGroup('1', self),
            '2': ComplexGroup('2', self),
            '3': ComplexGroup('3', self),
            '4': ComplexGroup('4', self)
        }

    def get_group(self, id):
        if id not in self.__groups:
            raise ValueError('invalid group, acceptable values 1, 2, 3, 4 or *')
        return self.__groups[id]

    def send(self, part_a, part_b):
        broadcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        broadcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        try:
            ops = bytearray()
            ops.append(part_a)
            ops.append(part_b)
            ops.append(0x55)

            broadcast_socket.sendto(ops, (self.__ip, self.__port))
            time.sleep(0.5)
        finally:
            broadcast_socket.close()
