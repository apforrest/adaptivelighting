import datetime

import requests

print datetime.datetime.now().isoformat() + ' turning lights on for the morning'

resp = requests.get('http://raspi.teamcurie.org:5000/light/group/1?status=on')
resp.raise_for_status()

requests.get('http://raspi.teamcurie.org:5000/light/group/1?colour=white')
requests.get('http://raspi.teamcurie.org:5000/light/group/1?brightness=100')